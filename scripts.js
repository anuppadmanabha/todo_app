(function() {

      var input = document.getElementById('input');
      var lists = {
         todo: document.getElementById('todo'),
         done: document.getElementById('done')
      };

      // This will create a list
      var createList = function(str, onCheck) {
	  
         var compItem = document.createElement('li');
		 var submitBtn = document.createElement('input');
         var label = document.createElement('span');

         submitBtn.type = 'button';
		 submitBtn.value = 'done';
         submitBtn.addEventListener('click', onAddList);
         label.textContent = str;

         compItem.appendChild(submitBtn);
         compItem.appendChild(label);
		 
		 return compItem;
      };

      var addTask = function(task) {
         lists.todo.appendChild(task);
      };

      var onAddList = function(event) {
         var task = event.target.parentElement;
         var type = task.parentElement.id;

		 if(type == 'done'){
			//console.log(type);
			lists['todo'].appendChild(task);
			this.style.visibility = 'hidden';
			input.focus();
		 }
		 else{
			//console.log(type);
			lists['done'].appendChild(task);
			this.style.visibility = 'hidden';
			input.focus();
		 }
      };

      var onInput = function() {
         var str = input.value.trim();
		 //console.log(str);
         if (str.length > 0) {
            addTask(createList(str, onAddList));
            input.value = '';
            input.focus();
         }
      };

      input.addEventListener('keyup', function(event) {
		  //console.log(event.keyCode);
		 //Check if its Enter
         if (event.keyCode === 13) {
            onInput();
         }
      });

      input.focus();
 }());